
INSERT
INTO z_product_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_PRODUCT_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_customer_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_CUSTOMER_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_subscription_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_subscription_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_AMENDMENT_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_amendment_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_invoice_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_invoice_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_invoice_item_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_invoice_item_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_payment_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_payment_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_credit_balance_adj_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_cb_adj_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_rate_plan_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_rate_plan_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_rate_plan_charge_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_rate_plan_charge_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_refund_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_refund_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_invoice_payment_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_invoice_payment_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
INSERT
INTO z_invoice_adjustment_batches
  (
    batch_id,
    RECORDS_QUERIED ,
    RECORDS_TRANSFERRED ,
    min_trx_creation_date ,
    max_trx_creation_date ,
    min_trx_last_update_date ,
    max_trx_last_update_date ,
    processing_status ,
    creation_date ,
    last_update_date
  )
  VALUES
  (
    Z_invoice_adj_batches_S.nextval,
    0,0,
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    to_date('01-JAN-2041','dd-MON-YYYY'),
    'LC',
    sysdate,
    sysdate
  )
/
*/
SELECT * FROM Z_AMENDMENT_BATCHES ORDER BY BATCH_ID DESC
/
select count(1) from z_amendments where batch_id > 168
/
--DELETE FROM Z_AMENDMENT_BATCHES WHERE BATCH_ID > = 260
/
SELECT * FROM Z_CREDIT_BALANCE_ADJ_BATCHES ORDER BY BATCH_ID DESC
/
--DELETE FROM Z_CREDIT_BALANCE_ADJ_BATCHES WHERE BATCH_ID > = 80
/
SELECT * FROM Z_CUSTOMER_BATCHES ORDER BY BATCH_ID DESC
/
--delete from z_customer_batches where batch_id > = 237
/
SELECT count(1) FROM Z_CUSTOMERS where batch_id > 228
/
--DELETE FROM Z_CUSTOMER_BATCHES WHERE BATCH_ID > = 231
/
--delete from z_customers where batch_id > 231
/
SELECT * FROM Z_INVOICE_ADJUSTMENT_BATCHES ORDER BY BATCH_ID DESC
/
select count(1) from z_invoice_adjustments where batch_id > 75
/
--DELETE FROM Z_INVOICE_ADJUSTMENT_BATCHES WHERE BATCH_ID > = 76
/
SELECT * FROM Z_INVOICE_BATCHES ORDER BY BATCH_ID DESC
/
truncate table kmule.Z_INVOICE_BATCHES
/
truncate table kmule.z_invoices
/
select count(1) from z_invoices where batch_id > 238
/
--DELETE FROM Z_INVOICE_BATCHES WHERE BATCH_ID > = 173
/
SELECT * FROM Z_INVOICE_ITEM_BATCHES ORDER BY BATCH_ID DESC
/
delete from Z_INVOICE_ITEM_BATCHES where batch_id > 740
/
select count(1) from z_invoice_items where batch_id >740
/
delete from z_invoice_items where batch_id >740
/
select count(1) from z_invoice_items where batch_id > 262
/
SELECT * FROM Z_INVOICE_PAYMENT_BATCHES ORDER BY BATCH_ID DESC
/
select count(1) from z_invoice_payments where batch_id > 240
/
--DELETE FROM Z_INVOICE_PAYMENT_BATCHES WHERE BATCH_ID > = 125
/
SELECT * FROM Z_PAYMENT_BATCHES ORDER BY BATCH_ID DESC
/
--DELETE FROM Z_PAYMENT_BATCHES WHERE BATCH_ID > = 157
/
SELECT * FROM Z_PRODUCT_BATCHES ORDER BY BATCH_ID DESC
/
--DELETE FROM Z_PRODUCT_BATCHES WHERE BATCH_ID > = 12
/
SELECT * FROM Z_RATE_PLAN_BATCHES ORDER BY BATCH_ID DESC
/
--delete FROM Z_RATE_PLAN_BATCHES where BATCH_ID > 155
/
select count(1) from z_rate_plan where batch_id > 238
/
--DELETE FROM z_rate_plan WHERE BATCH_ID > 155
/
SELECT * FROM Z_RATE_PLAN_CHARGE_BATCHES ORDER BY BATCH_ID DESC --12:36 PM EST
/
select * from z_rate_plan_charge_errors order by 1 desc
/
--delete FROM Z_RATE_PLAN_CHARGE_BATCHES where BATCH_ID > 164
/
select count(1) from z_rate_plan_charge where batch_id > 164
/
--DELETE FROM Z_RATE_PLAN_CHARGE WHERE batch_id > 164
/
SELECT * FROM Z_REFUND_BATCHES ORDER BY BATCH_ID DESC
/
--DELETE FROM Z_REFUND_BATCHES WHERE BATCH_ID > = 131
/
SELECT * FROM Z_SUBSCRIPTION_BATCHES ORDER BY BATCH_ID DESC
/
--delete from z_subscription_batches where batch_id = 302
/
select count(1) from z_subscriptions where batch_id > 199
/
--delete from z_subscriptions where batch_id >= 304
/
--DELETE FROM Z_SUBSCRIPTION_BATCHES WHERE BATCH_ID > = 304


