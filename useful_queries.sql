--Create the Initial Purchase

--Initial Transaction

SELECT trx_map.trx_code,
  trx_map.ar_trx_type_name,
  trx_map.revenue_impact,
  trx_map.monetary_trx,
  trx_map.tuition_trx, 
  trx_map.institutional,
  trx_map.hsg_rule,
  trx_line_map.line_group,
  trx_line_map.sku_override,
  trx_line_map.rev_rec_rule_override,
  trx_line_map.gl_account_override,
  trx_line_map.hsg_applies,
  trx_line_map.sort_order,
  subs.subscription_number,
  subs.z_created_date,
  hub_config.delta_k_hub_name,
  subs.version,
  subs.installment_billing_c,
  subs.activate_assets_c,
  subs.booking_id_c,
  subs.class_code_c,
  subs.purchase_id_c,
  subs.first_receipt_c,
  subs.product_code_c,
  subs.product_line_c,
  subs.revenue_center_c,
  subs.start_date_c,
  subs.end_date_c,
  subs.purchase_expiration_c,
  subs.promo_code_c,
  subs.channel_c,
  (case when trx_line_map.quantity_override = 'N' then
    1
  else 
    sum(lines.quantity)
  end) line_quantity,
  (case when trx_line_map.quantity_override = 'N' then
    sum(lines.charge_amount)
  else 
    sum(lines.charge_amount)/sum(lines.quantity)
  end) charge_amount
FROM z_subscriptions subs,
  z_customers cust,
  z_invoice_items lines,
  z_invoices inv,
  z_rate_plan_charge plan_charge,
  z_rate_plan plans,
  krr_zbill_trx_map trx_map,
  krr_zbill_trx_line_map trx_line_map,
  krr_hub_configs hub_config
WHERE subs.account_id               = cust.ORIG_SYSTEM_REFERENCE
--AND cust.student_id                 = '1527633330' --'1527225252'
AND lines.subscription_id           = subs.subscription_id
AND plan_charge.rate_plan_charge_id = lines.rate_plan_charge_id
AND plans.rate_plan_id              = plan_charge.rate_plan_id
AND plans.subscription_id           = subs.subscription_id
AND inv.invoice_id                  = lines.invoice_id
and lines.charge_amount <> 0
AND trx_map.trx_code                = subs.transaction_code_c
AND trx_map.map_id                  = trx_line_map.map_id
AND trx_line_map. rate_plan         = plan_charge.name
AND subs.delta_k_hub_c              = hub_config.delta_k_hub_code
--and hub_config.enabled_flag = 'Y'
AND trx_map.initial_enrollment = 'Y'
AND trx_map.transaction        = 'Y'
AND trx_map.enabled_flag       = 'Y'
--AND subs.version               = 1
and student_id = '1527647273'
group by trx_map.trx_code,
  trx_map.ar_trx_type_name,
  trx_map.revenue_impact,
  trx_map.monetary_trx,
  trx_map.tuition_trx, 
  trx_map.institutional,
  trx_map.hsg_rule,
  trx_line_map.line_group,
  trx_line_map.sku_override,
  trx_line_map.rev_rec_rule_override,
  trx_line_map.gl_account_override,
  trx_line_map.hsg_applies,
  trx_line_map.sort_order,
  subs.subscription_number,
  subs.z_created_date,
  hub_config.delta_k_hub_name,
  subs.version,
  subs.installment_billing_c,
  subs.activate_assets_c,
  subs.booking_id_c,
  subs.class_code_c,
  subs.purchase_id_c,
  subs.first_receipt_c,
  subs.product_code_c,
  subs.product_line_c,
  subs.revenue_center_c,
  subs.start_date_c,
  subs.end_date_c,
  subs.purchase_expiration_c,
  subs.promo_code_c,
  subs.channel_c,
  trx_line_map.quantity_override,
  lines.quantity
order by trx_line_map.sort_order 
/



--Initial Payments and Subsequent Payment
SELECT invpay.interface_id invpay_intf_id,
  invoice_payment_id,
  pay.interface_id pay_intf_id,
  pay.payment_id,
  pay.amount,
  EFFECTIVEDATE,
  payment_number,
  type,
  pay.z_created_date payment_z_created_date,
  check_number_c,
  journal_number_c,
  transaction_code_c,
  receipt_num_c,
  cybersource_token_c,
  credit_card_last4_c,
  purchase_id_c,
  cc_type_c,
  cust.orig_system_reference account_id,
  cust.student_id,
  trx_map.INITIAL_ENROLLMENT,
  trx_map.SUBSEQUENT_TRX
FROM z_invoice_payments invpay,
  z_payments pay,
  z_customers cust,
  krr_zbill_trx_map trx_map
WHERE invpay.payment_id        = pay.payment_id
AND cust.orig_system_reference = pay.account_id
AND pay.TRANSACTION_CODE_C     = trx_map.trx_code
AND trx_map.payment            = 'Y'
AND invpay.process_status      = 'LC'
AND pay.process_status         = 'LC'
AND cust.process_status       <> 'LD'
/
--Below Query can be used for 466 (up/down), 455(up/down), 475 (up/down), 476 (down), 481 (up/down), 482 (up), 463 (down)  --Need validation for the type of adjustment being made
SELECT subs1.interface_id sub1_int_id,
  subs1.subscription_id sub1_subscription_id,
  subs1.version sub1_version,
  amd.interface_id amd_int_id,
  amd.amendment_id amd_amendment_id,
  amd.code,
  amd.effective_date,
  amd.creation_date,
  amd.name,
  inv2.interface_id inv2_int_id,
  inv2.invoice_id inv2_int_id,
  inv2.invoice_number inv2_invoice_number,
  inv2.transaction_id_c transaction_id,
  inv2.receipt_num_c receipt_number,
  inv2.amount,
  lines2.interface_id lines2_int_id,
  lines2.CHARGE_AMOUNT,
  subs2.interface_id SUBS2_INT_ID,
  subs2.subscription_id subs2_subscription_id,
  subs2.installment_billing_c,
  subs2.activate_assets_c,
  subs2.enrollment_status_c,
  subs2.journal_number_c,
  subs2.transaction_batch_id_c,
  subs2.purchase_id_c,
  subs2.first_receipt_c,
  subs2.RECEIPT_NUM_C,
  subs2.transaction_id_c,
  subs2.transaction_code_c,
  subs2.product_code_c,
  subs2.product_line_c,
  subs2.revenue_center_c,
  subs2.start_date_c,
  subs2.end_date_c,
  subs2.purchase_expiration_c,
  subs2.promo_code_c,
  subs2.channel_c,
  subs2.process_status,
  subs2.z_created_date,
  cust.student_id,
  cust.orig_system_reference
FROM z_amendments amd,
  z_subscriptions subs2,
  z_invoices inv2,
  z_invoice_items lines2,
  z_customers cust,
  z_subscriptions subs1,
  Krr_Zbill_Trx_Map trx_map
WHERE amd.subscription_id      = subs1.subscription_id
AND cust.ORIG_SYSTEM_REFERENCE = subs2.account_id
AND subs1.subscription_number  = subs2.subscription_number
AND inv2.invoice_id            = lines2.invoice_id
AND lines2.subscription_id     = subs2.subscription_id
AND lines2.SKU                 = 'ADJ'
AND subs1.version              = subs2.version - 1
AND amd.name                   = subs2.transaction_code_c
AND amd.name                   = trx_map.TRX_CODE
AND trx_map.adjustment         = 'Y'
AND amd.process_status         = 'LC'
AND inv2.process_status        = 'LC'
AND lines2.process_status      = 'LC'
AND subs2.process_status       = 'LC'
AND subs2.process_status      <> 'LD'
/
--CSE:
SELECT amend.interface_id amd_intid,
  amend.amendment_id,
  amend.code,
  amend.name,
  amend.effective_date,
  amend.z_created_date,
  subs1.interface_id subs1_intf_id,
  subs1.subscription_id subs1_subsid,
  subs1.version subs1_version,
  subs2.interface_id subs2_intid,
  subs2.subscription_id subs2_subsid,
  subs2.version subs2_version,
  subs2.notes,
  subs2.customer_service_event_type_c,
  subs2.activate_assets_c,
  subs2.enrollment_status_c,
  subs2.is_non_tution_c,
  subs2.term_type_c,
  subs2.DELTA_K_HUB_C,
  subs2.booking_id_c,
  subs2.class_code_c,
  subs2.purchase_id_c
FROM z_amendments amend,
  z_subscriptions subs1,
  z_subscriptions subs2,
  krr_zbill_trx_map trx_map
WHERE amend.subscription_id             = subs1.subscription_id
AND subs1.account_id                    = subs2.account_id
AND subs1.version                       = subs2.version - 1
AND subs1.subscription_number           = subs2.subscription_number
AND amend.name                          = 'CSE'
AND Subs2.Customer_Service_Event_Type_C = Trx_Map.Trx_Code
AND Trx_Map.Cust_Service_Event          = 'Y'
AND subs1.process_status               <> 'LD'
AND amend.process_status                = 'LC'
AND subs2.process_status                = 'LC'
UNION
SELECT amend.interface_id amd_intid,
  amend.amendment_id,
  amend.code,
  amend.name,
  amend.effective_date,
  amend.z_created_date,
  subs1.interface_id subs1_intf_id,
  subs1.subscription_id subs1_subsid,
  subs1.version subs1_version,
  subs2.interface_id subs2_intid,
  subs2.subscription_id subs2_subsid,
  subs2.version subs2_version,
  subs2.notes,
  subs2.customer_service_event_type_c,
  subs2.activate_assets_c,
  subs2.enrollment_status_c,
  subs2.is_non_tution_c,
  subs2.term_type_c,
  subs2.DELTA_K_HUB_C,
  subs2.booking_id_c,
  subs2.class_code_c,
  subs2.purchase_id_c
FROM z_amendments amend,
  z_subscriptions subs1,
  z_subscriptions subs2,
  krr_zbill_trx_map trx_map
WHERE amend.subscription_id    = subs1.subscription_id
AND subs1.account_id           = subs2.account_id
AND subs1.version              = subs2.version - 1
AND subs1.subscription_number  = subs2.subscription_number
AND amend.name                 = Subs2.transaction_code_c
AND Subs2.transaction_code_c   = Trx_Map.Trx_Code
AND Trx_Map.Cust_Service_Event = 'Y'
AND subs1.process_status      <> 'LD'
AND amend.process_status       = 'LC'
AND subs2.process_status       = 'LC'
/

select * from z_products prod, krr_program_hsg_rates rates
where prod.PROGRAM_SHORT_NAME__C = rates.PROGRAM_SHORT_NAME(+)
/
--Refunds

SELECT adj_refund.interface_id adj_int_id,
  cb_adjustment_id,
  adjustment_date,
  adjustment_number,
  adj_refund.z_created_date adj_z_created_date,
  refund.interface_id ref_int_id,
  refund.refund_id,
  amount refund_amount,
  CASE
    WHEN method_type = 'CreditCard'
    THEN 'Credit Card'
    WHEN method_type = 'Other'
    THEN 'Check'
  END payment_method,
  refund_date,
  refund.z_created_date refund_z_crated_date,
  purchase_id_c,
  transaction_code_c,
  receipt_num_c,
  Journal_Number_c,
  cust.student_id,
  cust.ORIG_SYSTEM_REFERENCE account_id
FROM z_credit_balance_adj adj_refund,
  z_customers cust,
  z_refunds refund,
  krr_zbill_trx_map trx_map
WHERE adj_refund.account_id            = cust.orig_system_reference
AND adj_refund.source_transaction_type = 'Refund'
AND adj_refund.source_transaction_id   = refund.refund_id
AND refund.transaction_code_c          = trx_map.trx_code--'416' --416, 477,452,453, 491
AND trx_map.refund                     = 'Y'
/


--Deferred Revenue Queries for 429 transactions codes

SELECT gl_dist.*
FROM ra_customer_trx_all rct,
  ra_customer_trx_lines_all rctl,
  RA_CUST_TRX_LINE_GL_DIST_ALL gl_dist,
  apps.krr_purchases kp,
  apps.krr_customers kc,
  apps.krr_all_events ke
WHERE rct.customer_trx_id      = rctl.customer_trx_id
AND rctl.customer_trx_line_id  = gl_dist.customer_trx_line_id
AND gl_dist.account_class      = 'UNEARN'
AND gl_dist.POSTING_CONTROL_ID > 0
AND (kp.krr_purchase_id                                 = :P_PO_ID
OR kc.student_id                                        = :P_STUDENT_ID )
AND kp.krr_customer_id                                  = kc.krr_customer_id (+)
AND kp.org_id                                           = kc.org_id (+)
AND kp.krr_purchase_id                                  = ke.krr_purchase_id
AND ke.krr_event_id                                     = kt.krr_event_id
AND ke.krr_purchase_id                                  = kt.krr_purchase_id
AND ke.org_id                                           = kt.org_id
/


--DRST Reports
SELECT TO_CHAR(event_date,'MON-YYYY') period,
  journal_number_c,
  trx_code,
  event_class,
  COUNT(1),
  SUM(total_trx_amt),
  SUM(total_adj_amt),
  SUM(payment_amt),
  SUM(refund_amt)
FROM krr_all_events
GROUP BY TO_CHAR(event_date,'MON-YYYY'),
  journal_number_c,
  trx_code,
  event_class
/
SELECT TO_CHAR(event_date,'MON-YYYY') period,
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME,
  COUNT(1),
  SUM(total_trx_amt),
  SUM(total_adj_amt),
  SUM(payment_amt),
  SUM(refund_amt)
FROM krr_all_events evnt, krr_zbill_trx_map map, krr_purchases pur, krr_hub_configs hubs
where evnt.trx_code = map.trx_code
and pur.krr_purchase_id = evnt.krr_purchase_id
and pur.AREA_CODE = hubs.DELTA_K_HUB_CODE
and map.transaction = 'Y'
and evnt.event_class = 'Transaction'
and hubs.currency = 'USD'
GROUP by TO_CHAR(event_date,'MON-YYYY'),
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME
ORDER BY to_number(2), delta_k_hub_name  
/
SELECT TO_CHAR(event_date,'MON-YYYY') period,
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME,
  COUNT(1),
  SUM(total_trx_amt),
  SUM(total_adj_amt),
  SUM(payment_amt),
  SUM(refund_amt)
FROM krr_all_events evnt, krr_zbill_trx_map map, krr_purchases pur, krr_hub_configs hubs
where evnt.trx_code = map.trx_code
and pur.krr_purchase_id = evnt.krr_purchase_id
and pur.AREA_CODE = hubs.DELTA_K_HUB_CODE
and map.adjustment = 'Y'
and evnt.event_class = 'Adjustment'
and hubs.currency = 'USD'
GROUP by TO_CHAR(event_date,'MON-YYYY'),
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME
ORDER BY to_number(2), delta_k_hub_name    
/
SELECT TO_CHAR(event_date,'MON-YYYY') period,
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME,
  COUNT(1),
  SUM(total_trx_amt),
  SUM(total_adj_amt),
  SUM(payment_amt),
  SUM(refund_amt)
FROM krr_all_events evnt, krr_zbill_trx_map map, krr_purchases pur, krr_hub_configs hubs
where evnt.trx_code = map.trx_code
and pur.krr_purchase_id = evnt.krr_purchase_id
and pur.AREA_CODE = hubs.DELTA_K_HUB_CODE
and map.payment = 'Y'
and evnt.event_class = 'Payment'
and hubs.currency = 'USD'
GROUP by TO_CHAR(event_date,'MON-YYYY'),
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME
ORDER BY to_number(2), delta_k_hub_name  
/
SELECT TO_CHAR(event_date,'MON-YYYY') period,
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME,
  COUNT(1),
  SUM(total_trx_amt),
  SUM(total_adj_amt),
  SUM(payment_amt),
  SUM(refund_amt)
FROM krr_all_events evnt, krr_zbill_trx_map map, krr_purchases pur, krr_hub_configs hubs
where evnt.trx_code = map.trx_code
and pur.krr_purchase_id = evnt.krr_purchase_id
and pur.AREA_CODE = hubs.DELTA_K_HUB_CODE
and map.refund = 'Y'
and evnt.event_class = 'Refund'
and hubs.currency = 'USD'
GROUP by TO_CHAR(event_date,'MON-YYYY'),
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME
ORDER BY to_number(2), delta_k_hub_name   
/
SELECT TO_CHAR(event_date,'MON-YYYY') period,
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME,
  COUNT(1),
  SUM(total_trx_amt),
  SUM(total_adj_amt),
  SUM(payment_amt),
  SUM(refund_amt)
FROM krr_all_events evnt, krr_zbill_trx_map map, krr_purchases pur, krr_hub_configs hubs
where evnt.trx_code = map.trx_code
and pur.krr_purchase_id = evnt.krr_purchase_id
and pur.AREA_CODE = hubs.DELTA_K_HUB_CODE
and map.CUST_SERVICE_EVENT = 'Y'
and evnt.event_class = 'CSE'
and hubs.currency = 'USD'
GROUP by TO_CHAR(event_date,'MON-YYYY'),
  journal_number_c,
  evnt.trx_code,
  hubs.DELTA_K_HUB_NAME
ORDER BY to_number(2), delta_k_hub_name  
/

--Accounting Entries Report
SELECT pur.orig_system_purchase_id,
  pur.purchase_date,
  pur.revenue_ready,
  pur.assets_enabled,
  pur.books_shipped,
  pur.revenue_hold,
  pur.product_code,
  pur.cost_center,
  pur.area_code,
  evnt.trx_category,
  evnt.event_description,
  evnt.trx_code,
  evnt.total_trx_amt,
  evnt.total_adj_amt,
  evnt.payment_amt,
  evnt.refund_amt,
  evnt.sequence_num,
  evnt.journal_number_c,
  evnt.receipt_num_c,
  krrtrx.trx_number,
  krrtrx.accounting_rule_name,
  gl_dist.percent,
  amount,
  gl_date,
  gl_posted_date,
  posting_control_id,
  account_class,
  acctd_amount,
  account_set_flag,
  gcc.concatenated_segments,
  gcc.gl_account_type,
  gl_dist.org_id,
  case when gl_dist.account_class= 'REC' then 1
      when gl_dist.account_class= 'UNEARN' then 2
      when gl_dist.account_class= 'REV' then 3
  end as acctd_class_sort
FROM krr_purchases pur,
  krr_transactions krrtrx,
  krr_all_events evnt,
  apps.ra_cust_trx_line_gl_dist_all gl_dist,
  apps.gl_code_combinations_kfv gcc
WHERE evnt.krr_purchase_id      = pur.krr_purchase_id
AND evnt.krr_event_id           = krrtrx.krr_event_id
AND gl_dist.customer_trx_id     = krrtrx.ra_cust_trx_id
AND gl_dist.CODE_COMBINATION_ID = gcc.code_combination_id
AND ra_cust_trx_id             IS NOT NULL
and pur.orig_system_purchase_id = '1031929340'
order by orig_system_purchase_id, evnt.sequence_num, gl_dist.org_id, krrtrx.krr_trx_id, acctd_class_sort
/

--revenue aging
SELECT kp.orig_system_purchase_id PO_ID,
  TO_CHAR(kp.purchase_date,'DD-MON-YYYY') PO_DATE,
  kp.product_code PRODUCT,
  kc.student_id STD_ID,
  kc.customer_name STD_NAME,
  TO_CHAR(ke.event_date,'DD-MON-YYYY') EVENT_DATE,
  ke.event_class EVENT_CLASS,
  (SELECT trx_description
  FROM apps.krr_zbill_trx_map
  WHERE ar_trx_type_name = kt.cust_trx_type_name
  ) TRX_CODE,
  kt.trx_number TRX_NUMBER,
  (SELECT SUM(gl_dist.acctd_amount)
  FROM apps.ra_customer_trx_lines_all rctl,
    apps.ra_rules rr,
    apps.ra_cust_trx_line_gl_dist_all gl_dist
  WHERE 1                       = 1
  AND rctl.customer_trx_id      = kt.ra_cust_trx_id
  AND rctl.customer_trx_line_id = gl_dist.customer_trx_line_id
  AND rctl.accounting_rule_id   = rr.rule_id
  AND rr.name                   = ksp.attribute2
    --AND gl_dist.POSTING_CONTROL_ID > 0
  ) TOTAL_REVENUE,
  (SELECT SUM(gl_dist.acctd_amount)
  FROM apps.ra_customer_trx_lines_all rctl,
    apps.ra_rules rr,
    apps.ra_cust_trx_line_gl_dist_all gl_dist
  WHERE 1                       = 1
  AND rctl.customer_trx_id      = kt.ra_cust_trx_id
  AND rctl.customer_trx_line_id = gl_dist.customer_trx_line_id
  AND rctl.accounting_rule_id   = rr.rule_id
  AND rr.name                   = ksp.attribute2
  AND gl_dist.account_class     = 'UNEARN'
    --AND gl_dist.POSTING_CONTROL_ID > 0
  ) TOTAL_UNREC_REVENUE,
  (SELECT MAX(gl_dist.gl_date)
  FROM apps.ra_customer_trx_lines_all rctl,
    apps.ra_rules rr,
    apps.ra_cust_trx_line_gl_dist_all gl_dist
  WHERE 1                       = 1
  AND rctl.customer_trx_id      = kt.ra_cust_trx_id
  AND rctl.customer_trx_line_id = gl_dist.customer_trx_line_id
  AND rctl.accounting_rule_id   = rr.rule_id
  AND rr.name                   = ksp.attribute2
    --AND gl_dist.account_class      = 'UNEARN'
    --AND gl_dist.POSTING_CONTROL_ID > 0
  ) LATEST_REVENUE_DATE
FROM krr.krr_system_parameters ksp,
  apps.krr_purchases kp,
  apps.krr_customers kc,
  apps.krr_all_events ke,
  apps.krr_transactions kt
WHERE 1        = 1
AND ksp.org_id = 425
AND ksp.org_id = kp.org_id
  --AND (kp.krr_purchase_id             = :P_PO_ID
  --OR kc.student_id            = :P_STUDENT_ID )
AND kp.krr_customer_id = kc.krr_customer_id (+)
--AND kp.org_id          = kc.org_id (+)
AND kp.krr_purchase_id = ke.krr_purchase_id
AND ke.krr_event_id    = kt.krr_event_id
AND ke.krr_purchase_id = kt.krr_purchase_id
--AND ke.org_id          = kt.org_id
/
--Revenue/Unearned Revenue
SELECT 
  (select delta_k_hub_name from krr_hub_configs hubs where hubs.delta_k_hub_code = kp.area_code) HUB_NAME,
   kp.orig_system_purchase_id PO_ID,
  TO_CHAR(kp.purchase_date,'DD-MON-YYYY') PO_DATE,
  kp.product_code PRODUCT,
  (SELECT SKU FROM APPS.KRR_PRODUCTS WHERE sku = kp.product_code AND rownum = 1
  ) PROG_NAME,
  kc.student_id STD_ID,
  kc.customer_name STD_NAME,
  ke.krr_event_id EVENT_ID,
  ke.TRX_CATEGORY EVENT_CATEGORY,
  TO_CHAR(ke.event_date,'DD-MON-YYYY') EVENT_DATE,
  ke.trx_code TRX_CODE,
  ke.event_class EVENT_CLASS,
  (SELECT trx_description
  FROM apps.krr_zbill_trx_map
  WHERE ar_trx_type_name = kt.cust_trx_type_name
  ) TRX_CODE_DESC,
  kt.HSG_TRX,
  kt.trx_number TRX_NUMBER,
  xet.name TRX_TYPE,
  xah.period_name PERIOD,
  TO_CHAR(xah.accounting_date,'DD-MON-YYYY') ACCT_DATE,
  DECODE(xah.gl_transfer_status_code , 'Y', 'Yes', 'No') POSTED_GL,
  xal.accounting_class_code ACCT_CLASS,
  gcc.concatenated_segments CC_CODE,
  xal.currency_code CUR_CODE,
  xal.accounted_dr DEBIT,
  xal.accounted_cr CREDIT
FROM apps.krr_purchases kp,
  apps.krr_customers kc,
  apps.krr_all_events ke,
  apps.krr_transactions kt,
  apps.xla_transaction_entities_upg xte,
  apps.xla_event_types_tl xet,
  apps.xla_ae_headers xah ,
  apps.xla_ae_lines xal,
  apps.gl_code_combinations_kfv gcc
WHERE 1                                                 = 1
--AND (kp.krr_purchase_id                                 = :P_PO_ID
--OR kc.student_id                                        = :P_STUDENT_ID )
AND kp.krr_customer_id                                  = kc.krr_customer_id (+)
--AND kp.org_id                                           = kc.org_id (+)
AND kp.krr_purchase_id                                  = ke.krr_purchase_id
AND ke.krr_event_id                                     = kt.krr_event_id
AND ke.krr_purchase_id                                  = kt.krr_purchase_id
--AND ke.org_id                                           = kt.org_id
AND xte.ledger_id                                       = 1
AND xte.application_id                                  = 222
AND xte.entity_code                                     = 'TRANSACTIONS'
AND kt.ra_cust_trx_id                                   = NVL(xte.source_id_int_1, -99)
AND NVL(xte.source_id_int_2,                       -99) = -99
AND NVL(xte.source_id_int_3,                       -99) = -99
AND NVL(xte.source_id_int_4,                       -99) = -99
AND xte.entity_id                                       = xah.entity_id
AND xte.ledger_id                                       = xah.ledger_id
AND xte.entity_code                                     = xet.entity_code
AND xte.application_id                                  = xet.application_id
AND xet.event_type_code                                 = xah.event_type_code
AND xah.ae_header_id                                    = xal.ae_header_id
AND xah.application_id                                  = xal.application_id
AND xal.code_combination_id                             = gcc.code_combination_id
UNION
SELECT (select delta_k_hub_name from krr_hub_configs hubs where hubs.delta_k_hub_code = kp.area_code) HUB_NAME,
kp.orig_system_purchase_id PO_ID,
  TO_CHAR(kp.purchase_date,'DD-MON-YYYY') PO_DATE,
  kp.product_code PRODUCT,
  (SELECT SKU FROM APPS.KRR_PRODUCTS WHERE sku = kp.product_code AND rownum = 1
  ) PROG_NAME,
  kc.student_id STD_ID,
  kc.customer_name STD_NAME,
  ke.krr_event_id EVENT_ID,
  ke.trx_category EVENT_CATEGORY,
  TO_CHAR(ke.event_date,'DD-MON-YYYY') EVENT_DATE,
  ke.trx_code TRX_CODE,
  ke.event_class EVENT_CLASS,
  (SELECT trx_description
  FROM apps.krr_zbill_trx_map
  WHERE trx_code = kt.transaction_code_c
  ) TRX_CODE_DESC,
  'N' HSG_TRX,
  kt.receipt_number TRX_NUMBER,
  xet.name TRX_TYPE,
  xah.period_name PERIOD,
  TO_CHAR(xah.accounting_date,'DD-MON-YYYY') ACCT_DATE,
  DECODE(xah.gl_transfer_status_code , 'Y', 'Yes', 'No') POSTED_GL,
  xal.accounting_class_code ACCT_CLASS,
  gcc.concatenated_segments CC_CODE,
  xal.currency_code CUR_CODE,
  xal.accounted_dr DEBIT,
  xal.accounted_cr CREDIT
FROM apps.krr_purchases kp,
  apps.krr_customers kc,
  apps.krr_all_events ke,
  apps.krr_payments kt,
  apps.xla_transaction_entities_upg xte,
  apps.xla_event_types_tl xet,
  apps.xla_ae_headers xah ,
  apps.xla_ae_lines xal,
  apps.gl_code_combinations_kfv gcc
WHERE 1                                                 = 1
--AND (kp.krr_purchase_id                                 = :P_PO_ID
--OR kc.student_id                                        = :P_STUDENT_ID )
AND kp.krr_customer_id                                  = kc.krr_customer_id (+)
--AND kp.org_id                                           = kc.org_id (+)
AND kp.krr_purchase_id                                  = ke.krr_purchase_id
AND ke.krr_event_id                                     = kt.krr_event_id
AND ke.krr_purchase_id                                  = kt.krr_purchase_id
--AND ke.org_id                                           = kt.org_id
AND xte.ledger_id                                       = 1
AND xte.application_id                                  = 222
AND xte.entity_code                                     = 'RECEIPTS'
AND kt.cash_receipt_id                                  = NVL(xte.source_id_int_1, -99)
AND NVL(xte.source_id_int_2,                       -99) = -99
AND NVL(xte.source_id_int_3,                       -99) = -99
AND NVL(xte.source_id_int_4,                       -99) = -99
AND xte.entity_id                                       = xah.entity_id
AND xte.ledger_id                                       = xah.ledger_id
AND xte.entity_code                                     = xet.entity_code
AND xte.application_id                                  = xet.application_id
AND xet.event_type_code                                 = xah.event_type_code
AND xah.ae_header_id                                    = xal.ae_header_id
AND xah.application_id                                  = xal.application_id
AND xal.code_combination_id                             = gcc.code_combination_id
ORDER BY 6,
  2,
  5,
  8,7,
  11,
  13,
  15
  /


--New Starts
SELECT Hubs.Delta_K_Hub_Name,
  Cust.Student_Id,
  Cust.Ship_To_Fname,
  Cust.Ship_To_Lname,
  trunc(Pur.Purchase_Date),
  Pur.Promo_Code,
  Pur.Initial_Purchase_Trx_Code,
  evnt.total_trx_amt,
  Pur.Institutional,
  Pur.Installment_Billing_C,
  Pur.Purchase_Expiration_Date,
  Pur.Hsg_Eligible,
  Pur.Hsg_Percent,
  Prod.Name,
  Prod.Hasonlineassets__C,
  Prod.Program_Short_Name__C
FROM krr_purchases pur,
  krr_products prod,
  krr_hub_configs hubs,
  krr_customers cust,
  krr_all_events evnt
WHERE evnt.krr_purchase_id = pur.krr_purchase_id
and Evnt.Event_Class = 'Transaction'
and evnt.trx_category = 'Initial'
and pur.krr_customer_id = cust.krr_customer_id
AND pur.product_code      = prod.sku
AND pur.area_code         = Hubs.Delta_K_Hub_Code
AND TRUNC(purchase_date) BETWEEN to_date('31-MAY-2015','dd-MON-YYYY') AND to_date('03-JUN-2015','dd-MON-YYYY')
order by purchase_date
/
SELECT arma.remit_bank_acct_use_id,
       arma.receipt_method_id,
       arma.creation_date,
       arbs.name batch_source_name,
       arbs.TYPE bach_source_type,
       arbs.description batch_source_desc,
       lkb.lockbox_number lockbox_number,
       arc.name receipt_class_name,
       arm.name receipt_method_name,
       arm.start_date receipt_method_start_date,
       arm.printed_name receipt_method_name,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = arma.cash_ccid) cash_acct,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = arma.earned_ccid) earned_disc_acct,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = arma.unapplied_ccid) unapplied_acct,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = arma.unearned_ccid) unearned_disc_acct,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = arma.on_account_ccid) on_account_acct,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = arma.unidentified_ccid) unidentified_acct,
       arma.start_date receipt_method_acct_start,
       bank_branch.bank_name,
       bank_branch.bank_branch_name,
       bank_branch.start_date bank_branch_start_date,
       accts.bank_account_name,
       accts.bank_account_num,
       accts.description account_desc,
       accts.masked_account_num,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = accts.asset_code_combination_id) bank_account_asset_acct,
       (select gcc.segment1||'.'||gcc.segment2||'.'||gcc.segment3||'.'||gcc.segment4||'.'||gcc.segment5||'.'||gcc.segment6||'.'||gcc.segment7 from gl_code_combinations gcc where gcc.code_combination_id = accts.cash_clearing_ccid) bank_account_cash_clearing,
       acct_uses.primary_flag
FROM ce_bank_accounts accts,
     ce_bank_acct_uses_all acct_uses,
     ce_bank_branches_v bank_branch,
     ar_receipt_method_accounts_all arma,
     ar_receipt_methods arm,
     ar_receipt_classes arc,
     ar_lockboxes_all lkb,
     ar_batch_sources_all arbs
WHERE     accts.bank_account_id = acct_uses.bank_account_id
      AND bank_branch.branch_party_id = accts.bank_branch_id
      AND arma.remit_bank_acct_use_id = acct_uses.bank_acct_use_id
      AND arm.receipt_method_id = arma.receipt_method_id
      AND arm.receipt_class_id = arc.receipt_class_id
      AND lkb.receipt_method_id = arm.receipt_method_id
      AND lkb.batch_source_id = arbs.batch_source_id
      AND arbs.default_receipt_class_id = arc.receipt_class_id
      AND arbs.default_receipt_method_id = arm.receipt_method_id
      AND arbs.remit_bank_acct_use_id = acct_uses.bank_acct_use_id
      AND arc.name = 'KTP ZUORA PAYMENTS'
      AND accts.ar_use_allowed_flag = 'Y'
      AND NVL (bank_branch.end_date, SYSDATE + 1) >= SYSDATE
      AND NVL (acct_uses.end_date, SYSDATE + 1) >= SYSDATE
      AND NVL (arma.end_date, SYSDATE + 1) >= SYSDATE
      AND NVL (arm.end_date, SYSDATE + 1) >= SYSDATE
      AND NVL (arbs.end_date_active, SYSDATE + 1) >= SYSDATE
      --and (lockbox_number like '%Precollege%' or lockbox_number like '%Law%' or lockbox_number like '%Business_Hub%' or lockbox_number like '%Med_Inter%' or lockbox_number like '%Health%')
order by lockbox_number
/
SELECT org_id,status,
  name,
  description,
  type,
  creation_sign,
  (select CONCATENATED_SEGMENTS FROM GL_CODE_COMBINATIONS_KFV GCC_KFV WHERE GCC_KFV.CODE_COMBINATION_ID =  gl_id_freight) freight,
  (select CONCATENATED_SEGMENTS FROM GL_CODE_COMBINATIONS_KFV GCC_KFV WHERE GCC_KFV.CODE_COMBINATION_ID =  gl_id_rec) RECEIVABLES,
  (select CONCATENATED_SEGMENTS FROM GL_CODE_COMBINATIONS_KFV GCC_KFV WHERE GCC_KFV.CODE_COMBINATION_ID =  gl_id_reV) REVENUE,
  (select CONCATENATED_SEGMENTS FROM GL_CODE_COMBINATIONS_KFV GCC_KFV WHERE GCC_KFV.CODE_COMBINATION_ID =  gl_id_clearing) CLEARING,
  (select CONCATENATED_SEGMENTS FROM GL_CODE_COMBINATIONS_KFV GCC_KFV WHERE GCC_KFV.CODE_COMBINATION_ID =  gl_id_TAX) tax,
  (select CONCATENATED_SEGMENTS FROM GL_CODE_COMBINATIONS_KFV GCC_KFV WHERE GCC_KFV.CODE_COMBINATION_ID =  gl_id_unbilled) UNBILLED,
  (select CONCATENATED_SEGMENTS FROM GL_CODE_COMBINATIONS_KFV GCC_KFV WHERE GCC_KFV.CODE_COMBINATION_ID =  gl_id_unearned) UNEARNED
FROM ra_cust_trx_types_all
WHERE org_id in (406, 407)
order by 1,3
/

SELECT kcust.student_id,
  kcust.customer_name,
  kpur.orig_system_purchase_id,
  kpur.initial_purchase_trx_code,
  kpur.purchase_date,
  kpur.revenue_ready,
  kpur.service_start_date,
  kpur.service_end_date,
  kpur.product_code,
  kpur.hsg_eligible,
  kpur.hsg_percent,
  kpur.area_code,
  kprodlin.product_line_short_name,
  kprodlin.sbu_name,
  kevent.trx_code,
  kevent.event_date,
  kevent.trx_category,
  kevent.event_class,
  kevent.status,
  kpur.COST_CENTER,
  kevent.error_text,
  kevent.sequence_num,
  kevent.transaction_id_c,
  kevent.journal_number_c,
  kevent.receipt_num_c,
  kevent.total_trx_amt,
  kevent.total_adj_amt,
  kevent.payment_amt,
  kevent.refund_amt
FROM krr_purchases kpur,
  krr_all_events kevent,
  krr_customers kcust,
  krr_product_lines kprodlin
WHERE kpur.krr_purchase_id   = kevent.krr_purchase_id
AND kcust.krr_customer_id    = kpur.krr_customer_id
AND kpur.ktp_product_line_id = kprodlin.ktp_product_line_id
--and kpur.orig_system_purchase_id = '2001067979'
ORDER BY kcust.student_id,
  kpur.orig_system_purchase_id,
  kevent.sequence_num
/
SELECT kcust.student_id,
  kcust.customer_name,
  kpur.orig_system_purchase_id,
  kpur.initial_purchase_trx_code,
  kpur.purchase_date,
  kpur.revenue_ready,
  kpur.service_start_date,
  kpur.service_end_date,
  kpur.product_code,
  kpur.hsg_eligible,
  kpur.hsg_percent,
  kpur.area_code,
  kprodlin.product_line_short_name,
  kprodlin.sbu_name,
  kevent.trx_code,
  kevent.event_date,
  kevent.trx_category,
  kevent.event_class,
  kevent.status,
  kpur.COST_CENTER,
  kevent.error_text,
  kevent.sequence_num,
  kevent.transaction_id_c,
  kevent.journal_number_c,
  kevent.receipt_num_c,
  kevent.total_trx_amt,
  kevent.total_adj_amt,
  kevent.payment_amt,
  kevent.refund_amt,
  ktrans.MONETARY_IMPACT,
  ktrans.REVENUE_IMPACT,
  ktrans.RECEIPT_NUM_C,
  ktrans.ORG_ID,
  ktrans.STATUS,
  ktrans.error_code
FROM krr_purchases kpur,
  krr_all_events kevent,
  krr_customers kcust,
  krr_product_lines kprodlin,
  krr_transactions ktrans
WHERE kpur.krr_purchase_id   = kevent.krr_purchase_id
AND kcust.krr_customer_id    = kpur.krr_customer_id
AND kpur.ktp_product_line_id = kprodlin.ktp_product_line_id
and ktrans.krr_purchase_id = kpur.krr_purchase_id
--and kpur.orig_system_purchase_id = '2001067979'
ORDER BY kcust.student_id,
  kpur.orig_system_purchase_id,
  kevent.sequence_num
/
--create database link KTPAREP.WORLD connect to FIN identified by "fin" using 'KTPAREP_PROD'; 
/
create database link KTPAREPQA connect to KBSSHARED identified by "KBSSHARED" using 'KBS_QA01_rds'
/
create synonym krr_ktp_rev_rec_trx for billing.z_ora_rev_rec_trx@KTPAREPQA
/
create synonym krr_ktp_rev_rec_adj for billing.z_ora_rev_rec_adj@KTPAREPQA
/
create synonym krr_ktp_rev_rec_pmt for billing.z_ora_rev_rec_pmt@KTPAREPQA
/
create synonym krr_ktp_rev_rec_rfnd for billing.z_ora_rev_rec_rfnd@KTPAREPQA
/
create synonym krr_ktp_rev_rec_cse for billing.z_ora_rev_rec_cse@KTPAREPQA
/
select * from krr_ktp_rev_rec_trx --where entry_date > to_date('06-JUL-2015','dd-MON-YYYY')
/
create or replace view krr_ktp_rev_rec_trx_v
as 
SELECT purchase_id,
  id transaction_id,
  area_id area_code,
  product_line_id,
  code trx_code,
  nvl(tuition_amount,0) + nvl(non_tuition_amount,0) trx_amount,
  entry_date event_date,
  journal_number,
  receipt_number
FROM krr_ktp_rev_rec_trx
/
SELECT area_code,
  product_line_id,
  trx_code,
  TRUNC(event_date) event_date,
  COUNT(1) trx_count,
  SUM(trx_amount) sum_trx_amount
FROM krr_ktp_rev_rec_trx_v
GROUP BY area_code,
  product_line_id,
  trx_code,
  TRUNC(event_date)
ORDER BY 4,2,3
/
CREATE OR REPLACE VIEW krr_ora_rev_rec_trx_v
AS
  SELECT kpur.orig_system_purchase_id purchase_id,
    kevent.transaction_id_c transaction_id,
    kpur.area_code,
    kprod_lin.KTP_PRODUCT_LINE_ID product_line_id,
    kevent.trx_code,
    total_trx_amt trx_amount,
    kevent.z_creation_date event_date,
    kevent.journal_number_c journal_number,
    kevent.RECEIPT_NUM_C receipt_number
  FROM krr_all_events kevent,
    krr_purchases kpur,
    krr_products kprod,
    krr_product_lines kprod_lin
  WHERE kpur.krr_purchase_id   = kevent.krr_purchase_id
  AND event_class              = 'Transaction'
  AND kpur.PRODUCT_CODE        = kprod.sku
  AND kprod.PRODUCT_LINE_ID__C = kprod_lin.KTP_PRODUCT_LINE_ID
  AND kevent.z_creation_date  >= subs_start_date
  AND kprod_lin.enabled_flag   = 'Y'
/
SELECT area_code,
  product_line_id,
  trx_code,
  TRUNC(event_date),
  COUNT(1),
  SUM(trx_amount)
FROM krr_ora_rev_rec_trx_v
GROUP BY area_code,
  product_line_id,
  trx_code,
  TRUNC(event_date)
